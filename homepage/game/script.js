let unitLength = 20;
let boxColor = '#000000';
let gridColor = 0
const strokeColor = '#000000';
let columns; /* To be determined by window width */
let rows; /* To be determined by window height */
let currentBoard;
let nextBoard;

let defaultClick = document.querySelector('#default')
let noGridClick = document.querySelector('#no-grid')
let squircleClick = document.querySelector('#squircle')
let circleClick = document.querySelector('#circle')

function setup() {
  /* Set the canvas to be under the element #canvas*/
  const canvas = createCanvas(windowWidth - 20, windowHeight * .6);
  canvas.parent(document.querySelector('#canvas'));
  /*Calculate the number of columns and rows */
  columns = floor((width) / unitLength);
  rows = floor((height) / unitLength);

  /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
  currentBoard = [];
  nextBoard = [];
  for (let i = 0; i < columns; i++) {
    currentBoard[i] = [];
    nextBoard[i] = []
  }
  // Now both currentBoard and nextBoard are array of array of undefined values.
  // Set the initial values of the currentBoard and nextBoard
  if (document.querySelector('#random').classList.contains("is-on")) {
    document.querySelector('#random').classList.remove('is-on')
    initRandom()
  } else if (document.querySelector('#wand').classList.contains("is-on")) {
    document.querySelector('#wand').classList.remove('is-on')
    initRainbow()
  } else {
    init()
  }
  //frame setup
  frameRate(24);
}

function hi() { console.log('hi') }
function init() {
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      currentBoard[i][j] = {
        isAlive: false,
        color: boxColor,
        age: 0,
      };
      nextBoard[i][j] = {
        isAlive: false,
        color: boxColor,
        age: 0,
      };
    }
  }
}

function initRandom() {
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      currentBoard[i][j] = {
        isAlive: rand(),
        color: boxColor,
        age: 0,
      };
      nextBoard[i][j] = {
        isAlive: rand(),
        color: boxColor,
        age: 0,
      };
    }
  }
}

function initRainbow() {
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      currentBoard[i][j] = {
        isAlive: rand(),
        color: Math.floor(Math.random() * 16777215).toString(16),
        age: 0,
      };
      nextBoard[i][j] = {
        isAlive: rand(),
        color: Math.floor(Math.random() * 16777215).toString(16),
        age: 0,
      };
    }
  }
}

function draw() {
  background(255);
  generate();
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      if (currentBoard[i][j].isAlive) {
        if (currentBoard[i][j].color === nextBoard[i][j].color) {
          let darkerColor;
          darkerColor = shadeColor(currentBoard[i][j].color, -(currentBoard[i][j].age));
          (currentBoard[i][j].age > 95) ? fill('#000000') : fill(darkerColor)
          console.log(darkerColor)
          console.log(currentBoard[i][j].age)
        } else {
          fill(currentBoard[i][j].color);
        }
      } else {
        fill(255);
      }
      strokeStatus()
      gridStyle(i, j)
    }
  }
}

//calculate darker color in Hex
function shadeColor(color, percent) {

  var R = parseInt(color.substring(1, 3), 16);
  var G = parseInt(color.substring(3, 5), 16);
  var B = parseInt(color.substring(5, 7), 16);

  R = parseInt(R * (100 + percent) / 100);
  G = parseInt(G * (100 + percent) / 100);
  B = parseInt(B * (100 + percent) / 100);

  R = (R < 255) ? R : 255;
  G = (G < 255) ? G : 255;
  B = (B < 255) ? B : 255;

  var RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
  var GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
  var BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));

  return "#" + RR + GG + BB;
}

function generate() {
  //Loop over every single box on the board
  for (let x = 0; x < columns; x++) {
    for (let y = 0; y < rows; y++) {
      // Count all living members in the Moore neighborhood(8 boxes surrounding)
      let neighbors = 0;
      let neighborsColor;
      for (let i of [-1, 0, 1]) {
        for (let j of [-1, 0, 1]) {
          if (i == 0 && j == 0) {
            // the cell itself is not its own neighbor
            continue;
          }
          // The modulo operator is crucial for wrapping on the edge
          let nx = (x + i + columns) % columns
          let ny = (y + j + rows) % rows
          let nCell = currentBoard[nx][ny]
          if (nCell.isAlive) {
            neighbors += 1
            neighborsColor = nCell.color
          }
        }
      }

      // with 2 & 3 neighbors => stable cell
      if (currentBoard[x][y].isAlive && (1 < neighbors) && (neighbors < 4)) {
        currentBoard[x][y].age++
      }
      // Rules of Life
      if (currentBoard[x][y].isAlive && neighbors < 2) {
        // Die of Loneliness nCell <= 1
        nextBoard[x][y].isAlive = false;
        nextBoard[x][y].age = 0
      } else if (currentBoard[x][y].isAlive && neighbors > 3) {
        // Die of Overpopulation nCell >= 4
        nextBoard[x][y].isAlive = false;
        nextBoard[x][y].age = 0
      } else if (!currentBoard[x][y].isAlive && neighbors == 3) {
        // New life due to Reproduction nCell = 3
        nextBoard[x][y].isAlive = true;
        nextBoard[x][y].color = neighborsColor;
      } else {
        // Stasis
        nextBoard[x][y] = {
          ...currentBoard[x][y]
        }
      }
    }
  }

  // Swap the nextBoard to be the current Board

  [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

/* When mouse is dragged */
function mouseDragged() {
  if (document.querySelector("#play").style.display === 'inline-block') {
    document.querySelector("#play").style.display = 'none';
    document.querySelector("#pause").style.display = 'inline-block';
  }
  /* If the mouse coordinate is outside the board */
  if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
    return;
  }
  const i = Math.floor(mouseX / unitLength);
  const j = Math.floor(mouseY / unitLength);
  currentBoard[i][j].isAlive = true;
  currentBoard[i][j].color = boxColor
  nextBoard[i][j].color = boxColor
  fill(boxColor);

  strokeStatus()
  gridStyle(i, j)
}

/* When mouse is pressed */
function mousePressed() {
  noLoop();
  mouseDragged();
}

/* When mouse is released */
function mouseReleased() {
  loop();
}

//reset the game
document.querySelector('#reset-game').addEventListener('click', () => init())

//resize 
window.onresize = function () {
  location.reload();
}

//play and pause game
function togglePlayAndPause() {
  let playButton = document.querySelector("#play")
  let pauseButton = document.querySelector("#pause")

  if (playButton.style.display === 'none') {
    noLoop();
    playButton.style.display = 'inline-block';
    pauseButton.style.display = 'none';
  } else if (playButton.style.display === 'inline-block') {
    playButton.style.display = 'none';
    loop();
    pauseButton.style.display = 'inline-block';
  };
}

//change fps rate
document.querySelector('#FPSrange')
  .addEventListener('ionChange', event => {
    let FPSrangeInput;
    FPSrangeInput = event.detail.value
    frameRate(FPSrangeInput)
    document.getElementById('fps').innerText = FPSrangeInput + ' FPS'
  })

//change scale
function scaleChange(input) {
  unitLength = input
}
document.querySelector('#scale')
  .addEventListener('ionChange', event => {
    let scaleValue;
    scaleValue = event.detail.value
    document.querySelector('#scale').setAttribute('range', scaleValue)
    scaleChange(scaleValue)
    setup()
  })

//color picked in the wheel
document.querySelector('#color-wheel').addEventListener('click', function () {
  (new EyeDropper()).open().then(function (result) {
    console.log(result.sRGBHex);
    boxColor = result.sRGBHex;
  });
});

//random color
document.querySelector('#wand').addEventListener('click', function (event) {
  event.currentTarget.classList.toggle('is-on')
  console.log(event.currentTarget.classList.contains('is-on'))
  setup()
})

// random init
document.querySelector('#random').addEventListener('click', function (event) {
  event.currentTarget.classList.toggle('is-on')
  console.log(event.currentTarget.classList.contains('is-on'))
  setup()
})

function rand() {
  return Math.random() > 0.8 ? true : false
}

defaultClick.addEventListener('click', function (event) {
  document.querySelector('#no-grid').classList.remove('is-on')
  document.querySelector('#squircle').classList.remove('is-on')
  document.querySelector('#circle').classList.remove('is-on')
})
document.querySelector('#no-grid').addEventListener('click', function (event) {
  event.currentTarget.classList.add('is-on')

})
document.querySelector('#squircle').addEventListener('click', function (event) {
  circleClick.classList.remove('is-on')
  event.currentTarget.classList.add('is-on')
  console.log(event.currentTarget)

})
document.querySelector('#circle').addEventListener('click', function (event) {
  squircleClick.classList.remove('is-on')
  event.currentTarget.classList.add('is-on')

})

function strokeStatus() {
  if (noGridClick.classList.contains('is-on')) return noStroke()
  return stroke(strokeColor);
}

function gridStyle(i, j) {
  if (squircleClick.classList.contains('is-on')) return rect(i * unitLength, j * unitLength, unitLength, unitLength, 8);
  if (circleClick.classList.contains('is-on')) return circle((i * unitLength)+(unitLength/2),(j * unitLength)+(unitLength/2), unitLength);
  return   rect(i * unitLength, j * unitLength, unitLength, unitLength);
}