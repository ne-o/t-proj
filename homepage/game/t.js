// Create an empty array mapArr.
// Loop through array elements.
// Call function mapFunc with the current element as the argument.
// Push the result of the mapFunc function to the mapArr array.
// Return the mapArr array after going through all the elements.

function myMap(array,func){
    let resultArray = []
    for(let index in array){
      for(let index in array){
          resultArray.push(func(array[index],index,array));
     }
    }
    return resultArray;
}


let x = [1,2,3,4]

console.log(myMap(x, x*2));