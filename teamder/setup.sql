CREATE DATABASE teamder;

CREATE USER teamder_admin WITH PASSWORD 'teamder' SUPERUSER;

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    username VARCHAR(30) UNIQUE NOT NULL,
    password CHAR(60) NOT NULL,
    nickname VARCHAR(20),
    age INTEGER,
    join_date TIMESTAMP NOT NULL default current_timestamp,
    profile_pic VARCHAR(255),
    description VARCHAR(255)
)

CREATE TABLE posts (
   id SERIAL primary key,
   creator_id VARCHAR(30) not null,
   category_id INTEGER not null,
   FOREIGN KEY (category_id) REFERENCES categories(id), 
   title VARCHAR(30) not null,
   content VARCHAR(255) not null,
   attachment VARCHAR(255) not null,
   created_at timestamp default current_timestamp
);


ALTER TABLE users ADD COLUMN district VARCHAR(255);
ALTER TABLE users ADD COLUMN gender CHAR(1);


CREATE TABLE categories (
    id SERIAL PRIMARY KEY,
    cat_name VARCHAR(255),
    cat_img VARCHAR(255)
);

INSERT INTO categories (cat_name,cat_img) VALUES 
('所有分類','cat01all.jpg')
,('電腦/資訊科技','cat02computer.jpg')
,('烹飪','cat03cooking.jpg')
,('運動','cat04sports.jpg')
,('語言','cat05language.jpg')
,('藝術/創意','cat06art.jpg')
,('音樂','cat07music.jpg')
,('其他','cat08others.jpg')
;

ALTER TABLE users ADD COLUMN iconImage VARCHAR(255);

CREATE TABLE relationship (
   id SERIAL primary key,
    origin_user_id INTEGER not null,
    target_user_id INTEGER not null
);


--<hard code for selecting followers>--

INSERT INTO relationship (origin_user_id , target_user_id) VALUES
(2,1),
(6,1),
(7,1)
;

INSERT INTO relationship (origin_user_id , target_user_id) VALUES
(1,6),
(1,7)
;
--<hard code for selecting followers>--

ALTER TABLE users ALTER COLUMN password drop not null;

DELETE FROM categories WHERE id=1;

ALTER TABLE posts ALTER COLUMN creator_id TYPE integer USING (creator_id::integer);


ALTER TABLE posts
    ADD CONSTRAINT fk_creator_id FOREIGN KEY (creator_id) REFERENCES users (id);

CREATE TABLE districts (
    id SERIAL PRIMARY KEY,
    district VARCHAR(255)
);

ALTER TABLE users ADD COLUMN district_id INTEGER REFERENCES districts(id);

INSERT INTO districts (district) VALUES 
('中西區')
,('東區')
,('南區')
,('灣仔區')
,('九龍城區')
,('觀塘區')
,('深水埗區')
,('黃大仙區')
,('油尖旺區')
,('離島區')
,('葵青區')
,('北區')
,('西貢區')
,('沙田區')
,('大埔區')
,('荃灣區')
,('屯門區')
,('元朗區')
;

CREATE TABLE matches (
   id SERIAL primary key,
   searching_id INTEGER not null,
   FOREIGN KEY (searching_id) REFERENCES users(id), 
   matching_id INTEGER not null,
   FOREIGN KEY (matching_id) REFERENCES users(id) 
);
CREATE TABLE chat_Room_Records (
   id SERIAL primary key,
   sender_id INTEGER not null,
   receiver_id INTEGER not null,
   messages VARCHAR(255) not null,
   created_at timestamp default current_timestamp,
   attachment VARCHAR(255),
   FOREIGN KEY (sender_id) REFERENCES users(id), 
   FOREIGN KEY (receiver_id) REFERENCES users(id)

);

--DEMO ONLY
INSERT INTO chat_Room_Records (sender_id,receiver_id,messages) VALUES 
(1,9,'hi');
INSERT INTO chat_Room_Records (sender_id,receiver_id,messages) VALUES 
(1,9,'HFAF');

INSERT INTO chat_Room_Records (sender_id,receiver_id,messages) VALUES 
(1,9,'bye');

INSERT INTO chat_Room_Records (sender_id,receiver_id,messages) VALUES 
(9,1,'AFA');

INSERT INTO chat_Room_Records (sender_id,receiver_id,messages) VALUES 
(9,1,'@$@$');

INSERT INTO chat_Room_Records (sender_id,receiver_id,messages) VALUES 
(1,7,'test1');

INSERT INTO chat_Room_Records (sender_id,receiver_id,messages) VALUES 
(1,7,'test2');
INSERT INTO chat_Room_Records (sender_id,receiver_id,messages) VALUES 
(7,1,'test3');

ALTER TABLE posts ADD COLUMN type CHAR(5);
--TEACH OR LEARN

UPDATE posts SET type = 'teach';

--TRUNCATE TABLE posts restart IDENTITY CASCADE;


--pick record to build a new table than join table--
select 
t1.id as record_id,
messages,
sender_id,
username,
iconimage
from (select max(id) as id
from chat_room_records where receiver_id = 1 group by sender_id) as t1 
join chat_room_records as crr on t1.id = crr.id 
join users on crr.sender_id = users.id
order by record_id desc;
--



ALTER TABLE matches RENAME TO dislikes;
 DELETE FROM chat_room_records 
            where chat_room_records.receiver_id = 1 and chat_room_records.sender_id = 9) and
            (chat_room_records.receiver_id = 9 and chat_room_records.sender_id = 1)            
ALTER TABLE users DROP COLUMN profile_pic;
ALTER TABLE users DROP COLUMN district_id;

ALTER TABLE users ADD COLUMN is_admin BOOLEAN;

--try get all users from users and turn to a table
    select id,
    '<tr><td>' 
    || string_agg(concat_ws('</td><td>','<button class="admin">OP</button><button class="member">M</button> ',id, COALESCE(is_admin, '0'), COALESCE(username, '0'), COALESCE(nickname, '0'), COALESCE(age,'0'), COALESCE(gender,'0'), COALESCE(district,'0'), COALESCE(join_date,'1900-1-1 00:00:00')), '</td></tr><tr><td>')
    || '</td></tr>' html
    from users
    group by id
    order by 1;

--update UNIQUE
CREATE TABLE blocklists (
   id SERIAL primary key,
    origin_user_id INTEGER not null,
    target_user_id INTEGER not null,
    UNIQUE (origin_user_id ,target_user_id )
);


--DEMO ONLY

INSERT INTO blocklists (origin_user_id , target_user_id) VALUES
(2,1),

DELETE FROM blocklists where origin_user_id = 1 and target_user_id =6;

ALTER TABLE users 
ADD CONSTRAINT ck_No_Special_Characters 
       CHECK (username NOT LIKE '%[^A-Z0-9a-z@]%');


ALTER TABLE users ADD COLUMN token CHAR(20);

ALTER TABLE relationship ADD UNIQUE (origin_user_id, target_user_id);

ALTER TABLE relationship RENAME TO followlists;

ALTER TABLE chat_room_records DROP COLUMN attachment;

ALTER TABLE users ADD COLUMN email;


CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
ALTER TABLE users ALTER COLUMN token TYPE uuid USING (token::uuid);

ALTER TABLE users 
ADD CONSTRAINT nickname_No_Special_Characters 
       CHECK (nickname ~* '%[^A-Z0-9a-z@]%');


--get the matches learn/teach
select username, age, district, gender, iconimage, learn, teach from users 
inner join (select creator_id, array_agg(title) as learn from posts where creator_id = 101 and type = 'learn' group by creator_id) as l on id = l.creator_id 
inner join (select creator_id, array_agg(title) as teach from posts where creator_id = 101 and type = 'teach' group by creator_id) as t on id = t.creator_id 
where id = 101;