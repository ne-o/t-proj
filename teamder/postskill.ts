import { unlink, fstat, mkdirSync } from "fs";
import formidable from "formidable";
import { client } from "./db";
import express, { Request, Response, NextFunction } from "express";
import path from "path";

export function deleteImage(folder: string, file: string) {
  let imgPath = __dirname + folder + file;
  unlink(path.resolve(imgPath), (err) => console.log(err));
}

export let postsRoutes = express.Router();
// postsRoutes.use(express.urlencoded({ extended: true }));
// postsRoutes.use(express.json());
// postsRoutes.use(express.static("uploads"));

postsRoutes.get("/postskillupdate", checkPermission);

let uploadDir = "uploads";
mkdirSync(uploadDir, { recursive: true });

const form = formidable({
  uploadDir,
  keepExtensions: true,
  maxFiles: 1,
  maxFileSize: 200 * 1024 ** 2, // the default limit is 200KB
  filter: (part) => part.mimetype?.startsWith("image/") || false,
});

export function extractSingleFile(
  file: formidable.File[] | formidable.File
): formidable.File {
  return Array.isArray(file) ? file[0] : file;
}

// export type Post = {
//   id: number
//   creator: number
//   skillid: number,
//   categoryid: number
//   title: string
//   content: string
//   attachment: string | null
//   createdate: string
// }

async function checkPermission(req: Request, res: Response) {
  console.log(req.session.user?.id);
  try {
    if (req.session.user?.id === undefined) {
      res.status(400).json({ error: "請先登入" });
    }
  } catch (e) {
    res.status(500).json({ error: String(e) });
  }
}

postsRoutes.post("/postskillupdate", async (req: Request, res: Response) => {
  // const {title,category,content} = req.body;
  // console.log(title)
  form.parse(req, async (err, fields, files: any) => {
    const file: any = extractSingleFile(files);
    if (Object.keys(file).length > 0) {
      const keys = Object.keys(file);
      await client.query(
        `INSERT INTO posts (creator_id,title,category_id,content,attachment,type) VALUES ($1,$2,$3,$4,$5,$6)`,
        [
          req.session.user?.id,
          fields.title,
          fields.category,
          fields.content,
          file[keys[0]].newFilename,
          fields["skill-radio"],
        ]
      );
      res.json({ success: true });
    } else {
      res.json({ error: "You must upload an attachment" });
    }
  });
});

postsRoutes.get("/skills/:id", (req, res) => {
  let id = +req.params.id;

  if (!id) {
    res.status(400).json({ error: "頁面不存在" });
    return;
  }

  client
    .query(
      /* sql */ `
  select
    posts.id
  , posts.creator_id
  , posts.title
  , posts.category_id
  , posts.content
  , posts.attachment
  , posts.created_at
  , username
  , iconimage
  , cat_name
  from posts 
  join users on users.id = posts.creator_id 
  join categories on categories.id = posts.category_id
  where posts.id = $1
  `,
      [id]
    )
    .then((result) => {
      let post = result.rows[0];
      if (!post) {
        res.status(404).json({ error: "Post not found" });
        return;
      }
      let userId = req.session.user?.id;
      let isAdmin = req.session.user?.isAdmin;
      res.json({ post, userId, isAdmin });
    })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});

postsRoutes.get("/postskill/:id", (req, res) => {
  let id = +req.params.id;
  if (!id) {
    res.status(400).json({ error: "Page not found" });
    return;
  }
  client
    .query(
      /* sql */ `
  select
    posts.id
  , posts.creator_id
  , posts.title
  , posts.category_id
  , posts.content
  , posts.type
  , posts.attachment
  from posts
  where posts.id = $1
  `,
      [id]
    )
    .then((result) => {
      let post = result.rows[0];
      if (result.rows[0].creator_id != req.session.user?.id) {
        res.status(404).json({ error: "喂你做咩改人地個POST！" });
        return;
      }
      if (!post) {
        res.status(404).json({ error: "頁面不存在" });
        return;
      }
      res.json(result.rows[0]);
    })
    .catch((error) => {
      res.status(500).json({ error: "頁面不存在" });
    });
});

postsRoutes.post("/postskillupdate/:id", (req: Request, res: Response) => {
  let user_id = req.session.user?.id;
  let id = +req.params.id;
  if (!id) {
    res.status(400).json({ error: "Missing id in req.params" });
    return;
  }
  // const {title,category,content} = req.body;
  form.parse(req, async (err, fields, files: any) => {
    const file: any = extractSingleFile(files);
    const keys = Object.keys(file);

    if (keys.length == 0) {
      await client
        .query(
          `UPDATE posts set title = $1, category_id = $2, content = $3, type=$4 WHERE id = $5 and creator_id = $6`,
          [
            fields.title,
            fields.category,
            fields.content,
            fields["skill-radio"],
            id,
            user_id,
          ]
        )
        .then((result) => {
          if (result.rowCount) {
            res.json({ ok: true });
          } else {
            res.status(400).json({
              error:
                "Failed to delete, only the creator of post can delete this post",
            });
          }
        })
        .catch((error) => {
          res.status(500).json({ error: String(error) });
        });
    } else {
      let existingImage = await client.query(
        "select attachment from posts where id  = $1",
        [id]
      );
      let imgPath = existingImage.rows[0].attachment;
      deleteImage("/uploads/", imgPath);
      let uploadedImage = file[keys[0]]?.newFilename || null;
      await client
        .query(
          `UPDATE posts set title = $1, category_id = $2, content = $3, type=$4, attachment = $5 WHERE id = $6 and creator_id = $7`,
          [
            fields.title,
            fields.category,
            fields.content,
            fields["skill-radio"],
            uploadedImage,
            id,
            user_id,
          ]
        )
        .then((result) => {
          if (result.rowCount) {
            res.json({ ok: true });
          } else {
            res.status(400).json({
              error:
                "Failed to delete, only the creator of post can delete this post",
            });
          }
        })
        .catch((error) => {
          res.status(500).json({ error: String(error) });
        });
    }
  });
});

postsRoutes.delete("/postskill/:id", (req, res) => {
  let post_id = +req.params.id;
  let user_id = req.session.user?.id;
  let isAdmin = req.session.user?.isAdmin;
  if (!post_id) {
    res.status(400).json({ error: "Missing id in req.params" });
    return;
  }
  client.query(
    "select creator_id, attachment from posts where id  = $1",
    [post_id]
  ).then((result) => {
    // console.log('result', result)
    let creatorId = result.rows[0].creator_id;
    let image = result.rows[0].attachment
    if (creatorId === user_id || isAdmin == true) {
      deleteImage('/uploads/', image)
      client
        .query(
      /* sql */ `
delete from posts
where id = $1
and (creator_id = $2 or $3)
`,
          [post_id, user_id, isAdmin]
        )
      res.json({ ok: true });

    } else {
      // .then((result) => {
      //   if (result.rowCount) {
      //     res.json({ ok: true });
      // } else {
      res.status(400).json({
        error:
          "Failed to delete, only the creator of post can delete this post",
      });
    }
  })
    // })
    .catch((error) => {
      res.status(500).json({ error: String(error) });
    });
});
