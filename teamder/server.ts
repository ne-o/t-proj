import { print } from "listening-on";
import { app } from "./app";
import http from "http";
import { env } from "./env";
import { attachServer } from "./io";

export let server = http.createServer(app);
attachServer(server);
console.log("Hello");
server.listen(env.PORT, () => {
  console.log("OK");
  print(env.PORT);
});
