let list = document.querySelector("table");
let adminButtons;
let memberButtons;

// console.log(list)

// window.onload = () => {
getMemberList();
// }

async function getMemberList() {
  let res = await fetch("/member");
  let result = await res.json();
  if (result.error) {
    alert(result.error);
    return;
  }
  // console.table(result.memberList.rows)
  let memberList = result.memberList.rows;
  // console.log(result.memberList.rows[0].html);

  // list.innerHTML = result.memberList.rows[0].html

  for (let i = 0; i < result.memberList.rows.length; i++) {
    list.innerHTML += result.memberList.rows[i].html;
  }
  adminButtons = document.querySelectorAll(".admin");
  memberButtons = document.querySelectorAll(".member");
  // console.log(adminButtons);

  // let promises = [];
  //     for (let i = 0; i < memberList.length; i++) {
  //         promises.push(fetch('/role/admin', {method: 'PUT', body: memberList[i].id}))
  //     }
  //     Promise.all(promises)
  //     .then(catch)

  for (let i = 0; i < memberList.length; i++) {
    let id = memberList[i].id;
    adminButtons[i].addEventListener("click", async (event) => {
      event.preventDefault();
      // console.log('give admin id:', id)
      const res = await fetch("/role/admin/" + id, {
        method: "PUT",
      });
      if (res.status !== 200) {
        const data = await res.json();
        alert(data.error);
        return;
      } else {
        window.location = "/admin/member.html";
      }
    });
    memberButtons[i].addEventListener("click", async (event) => {
      event.preventDefault();
      // console.log('clicked member:', id)
      const res = await fetch("/role/member/" + id, {
        method: "PUT",
      });
      if (res.status !== 200) {
        const data = await res.json();
        alert(data.error);
        return;
      } else {
        window.location = "/admin/member.html";
      }
    });
  }
}
