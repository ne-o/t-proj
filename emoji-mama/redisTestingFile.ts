import * as redis from 'redis'

let client = redis.createClient({
    url: 'redis://127.0.0.1:6379'
})

async function main() {
    await client.connect()
    await client.get('price')
        .then(value => { console.log(value) })
        .catch(e => { console.error(e) })
    await client.disconnect();
}
main()