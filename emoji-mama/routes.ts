import express, { Request, Response } from 'express';
import { print } from "listening-on";
import expressSession from 'express-session'
import { knex } from './app'
import { SinglePlayController } from './singlePlayController';
import path, { join, resolve } from 'path';
import { SinglePlayService } from './singlePlayService';



let app = express();

let singlePlayService = new SinglePlayService(knex)
let singlePlayController = new SinglePlayController(singlePlayService);
declare module 'express-session' {
    interface SessionData {
        cookie: Cookie
        image: string
    }

}
export let sessionMiddleware = expressSession({
    secret: 'Project 2',
    resave: true,
    saveUninitialized: true,
})


app.use(sessionMiddleware)

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.post('/lobby', singlePlayController.deleteImage)
app.get('/result', singlePlayController.getImage)
app.post('/getData', singlePlayController.getData)
app.get('/endGame', singlePlayController.endGame)
app.get('/rank', singlePlayController.getTopTenPlayers)
app.post('/getSpecialModeData', singlePlayController.getData)
app.post('/enterSpecialModeName', singlePlayController.enterName)
app.post('/enterName', singlePlayController.enterName)




const port = 8100;

app.use(express.static('public')) //
app.use(express.static("uploads")) //

app.use((_, res) => {
    res.sendFile(resolve(join("public", "404.html"))); //
});

app.listen(port, () => {
    print(port)
})