import Knex from 'knex';
import { SinglePlayService } from './singlePlayService';

const knexConfigs = require('./knexfile');
const configMode = process.env.NODE_ENV || 'development';
export const knex = Knex(knexConfigs[configMode]);


let singlePlayService = new SinglePlayService(knex);
singlePlayService.hello();
